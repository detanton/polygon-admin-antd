import { Outlet } from "react-router-dom";
import { Flex, Layout } from 'antd';
import { Navigation } from "../components/navigation";
import { Breadcrumbs } from "../components/breadcrumbs";
import './style.css';

const { Header, Content, Footer, Sider } = Layout;

export const Root = () => (
  <Flex gap="middle" wrap="wrap" className="flex-container">
    <Layout>
      <Sider width="300" className="">
        Sider
      </Sider>
      <Layout>
        <Header className="header">
          <Navigation />
        </Header>
        <Content className="content">
          <Breadcrumbs />
          <Outlet />
        </Content>
        <Footer>
          Polygon admin ©{new Date().getFullYear()}
        </Footer>
      </Layout>
    </Layout>
  </Flex>

);

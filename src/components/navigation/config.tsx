import { Link } from "react-router-dom";

export const items = [
  {
    label: <Link to="/">Dashboard</Link>,
    key: 'dashboard',
  },
  {
    label: <Link to="/about">About</Link>,
    key: 'about',
  },
];

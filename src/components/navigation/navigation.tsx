import { Menu } from "antd";
import { items } from "./config";
import { FC } from "react";
import './style.css';

export const Navigation: FC = () => (
  <Menu
    className="menu"
    mode="horizontal"
    defaultSelectedKeys={['dashboard']}
    items={items}
  />
);

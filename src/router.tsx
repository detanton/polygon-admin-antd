import { RouteObject } from "react-router-dom";
import { Root } from "./layouts/root";
import { About } from "./pages/about";
import { Home } from "./pages/home";

export const routerConfig: RouteObject[] = [
  {
    path: "/",
    element: <Root />,
    children: [
      {
        index: true,
        path: "/",
        element: <Home />,
      },
      {
        path: "/about",
        element: <About />,
      },
      {
        path: "*",
        element: <div>404</div>
      },
    ]
  },
]
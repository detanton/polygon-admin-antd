import { DatePicker } from 'antd';
import { FC } from 'react';

export const Home: FC = () => {

  return (
    <>
      <h1>Dashboard</h1>
      <DatePicker />
    </>
  );
};
